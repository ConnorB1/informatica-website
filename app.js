let express = require("express");
let port = 81;
let app = express();

app.get("/", (req, res) => {
  res.sendFile(`${__dirname}/index.html`);
});

app.use(express.static(`${__dirname}`));

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
